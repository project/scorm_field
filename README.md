# Scorm field

This module adds a scorm player field type that gives you the ability to add a scorm field to an entity. 

## Installation

Install the module like any other drupal module.

## Configration

Add a new field to your entity and choose "Scorm field". You will find it in the section "Reference fields".

### Decoupled mode

If you'd like to run the field in decoupled mode please visit to admin/config/system/scorm_field/settings

Please also create a view mode named "decoupled" for the entity to have attached the scorm field.

We recommend you install x_frame_options_configuration to allow your backend server to be accessed through an iframe on a different domain.


Endpoint to get a generated token.

Parameters: token (Token) (visit /admin/config/system/scorm_field/settings to generate token!)

Request: /api/scorm-field-scorm-get-access-token/{token}/{node_id}: GET

Response:

{
    "status": "success",
    "action": "token generated",
    "generated_token": "9503ce956440fb52",
    "iframe_source": "https://example.test/scorm-field-decoupled/1167/9503ce956440fb52/0"
}

Attention! Once the token has been used, it becomes invalid. So accessing the iframe source is only possible with a valid token. If you refresh the page, please trigger a new token for the iframe source.

Get Scorm Report:

Request: /api/scorm-field-scorm-report/{NID}: GET

Parameters: nid (int)
Filter by user simply attach ?user_uuid={UUID}

Response:

```

{
    "status": "success",
    "action": "scorm_report",
    "object": {
        "3": {
            "id": [
                {
                    "value": 3
                }
            ],
            "uuid": [
                {
                    "value": "9e3af963-2a54-4311-b363-40a309a8a6d6"
                }
            ],
            "uid": [
                {
                    "target_id": 3,
                    "target_type": "user",
                    "target_uuid": "44fc2be8-da2f-11e5-b5d2-0a1d41d68578",
                    "url": "/user/3/home"
                }
            ],
            "nid": [
                {
                    "target_id": 27,
                    "target_type": "node",
                    "target_uuid": "5519b5e5-3a49-4d99-9e49-ba3e77def843",
                    "url": "/node/27"
                }
            ],
            "created": [
                {
                    "value": "2024-06-17T12:06:38+00:00",
                    "format": "Y-m-d\\TH:i:sP"
                }
            ],
            "changed": [
                {
                    "value": "2024-06-17T12:06:38+00:00",
                    "format": "Y-m-d\\TH:i:sP"
                }
            ],
            "score_raw": [
                {
                    "value": 60
                }
            ],
            "score_max": [
                {
                    "value": 100
                }
            ],
            "score_min": [
                {
                    "value": 0
                }
            ],
            "status": [
                {
                    "value": "failed"
                }
            ],
            "ip": [
                {
                    "value": "xxx.xxx.xxx"
                }
            ],
            "session_uuid": []
        }
    }
}

```

Get Scorm Data:

Request: /api/scorm-data/{user_id}: GET

Parameters: user_id (string) UUID of the user
Filter by required nodes ?nids=[1,2,3,4]

Response:

```

{
    "status": "success",
    "action": "scorm_data",
    "data": {
        "3169": true,
        "3189": true
    }
}

```






