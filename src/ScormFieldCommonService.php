<?php

namespace Drupal\scorm_field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\scorm_field\ScormPlayerSettingsInterface;
use Drupal\scorm_field\Entity\ScormReport;
use Drupal\scorm_field\ScormReportInterface;
use Drupal\Core\Entity\EntityConstraintViolationList;

/**
 * Service description.
 */
class ScormFieldCommonService {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */  
  protected $entityFieldManager;

  /**
   * Constructs a ScormFieldService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager, 
    AccountInterface $account,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
    $this->entityFieldManager = $entity_field_manager;
  }

  public function scormFieldAvailable($entity_type, $bundle) {

    $field_name = FALSE;

    $fields = $this->entityFieldManager
      ->getFieldDefinitions($entity_type, $bundle);

    // Check for a scorm field
    foreach ($fields as $key => $field) {
      if ($field->getType() === 'scorm_field_scorm_package') {  
        $field_name = $field->getName();
      }
    }

    return $field_name;

  }

  /**
   * Get scorm report by Node Id filtered by user
   *
   * @param integer $nid
   *   The node id.
   * @param string $user_uuid
   *   The uuid of the user.
   * @return array $report
   */
  public function getScormReportByNodeId(int $nid, string $user_uuid = '') {

    $conditions['nid'] = $nid;

    // Let's get the user id from user_uuid
    $user = \Drupal::service('entity.repository')->loadEntityByUuid('user', $user_uuid);
    if ($user instanceof UserInterface || $user instanceof User) {
      $conditions['uid'] = $user->id();
    }

    $report = $this->entityTypeManager
      ->getStorage('scorm_report')
      ->loadByProperties($conditions);

    return $report;

  }  

  public function getScormDataForGivenNids(string $user_uuid, array $nids) {

    $results = [];
    
    if (isset($nids) && is_array($nids) && !empty($nids)) {
      $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
      if (!empty($nodes)) {
        foreach ($nodes as $node) {
          if ($node instanceof NodeInterface) {
            $scorm_report = $this->getScormReportByNodeId($node->id(), $user_uuid);
            if (!empty($scorm_report)) {
              $scorm_report_data = reset($scorm_report);
            }

            if (isset($scorm_report_data)) {
              if ($scorm_report_data instanceof ScormReportInterface) {
                if ($scorm_report_data->getStatus() === 'completed' || $scorm_report_data->getStatus() === 'passed') {
                  $results[$node->id()] = TRUE;
                }
                else {
                  $results[$node->id()] = FALSE;
                }
              }
              else {
                $results[$node->id()] = FALSE;  
              }

            }
          }
        }
      }
    }

    return $results;    

  } 

  /**
   * Check access to view scorm report
   *
   * @param integer $nid
   * @return $access false | true
   */
  public function checkViewReportAccess(int $nid) {

    $account = \Drupal::currentUser();

    $access = FALSE;

    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);

    if ($node instanceof NodeInterface) {
      $field_definitions = $node->getFieldDefinitions();
      foreach ($field_definitions as $machine_name => $field) {
        $className = $field->getClass();
        if ($className === '\Drupal\scorm_field\Plugin\Field\FieldType\ScormFieldScormPackageItemList') {
          if ($node->access('update', $account)) {
            $access = TRUE;
          }
        }        
      }
    }

    return $access;

  }

  public function getUserFromAccount(int $user_id) {
    $user = $this->entityTypeManager->getStorage('user')->load($user_id);
    return $user;
  }

  /**
   * Check access to view my scorm report
   *
   * @param integer $nid
   * @return $access false | true
   */  
  public function checkViewMyReportAccess(int $nid) {

    $account = \Drupal::currentUser();

    $access = FALSE;

    // If it's an anon. user we return early and deny access.
    if ($account->isAnonymous()) {
      return $access; 
    }

    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);

    if ($node instanceof NodeInterface) {
      $field_definitions = $node->getFieldDefinitions();
      foreach ($field_definitions as $machine_name => $field) {
        $className = $field->getClass();
        if ($className === '\Drupal\scorm_field\Plugin\Field\FieldType\ScormFieldScormPackageItemList') {
          if ($node->access('view', $account)) {
            $access = TRUE;
          }
        }        
      }
    }

    return $access;    

  }
  
  /**
   * Save scorm report.
   *
   * @param integer $nid
   *   The nid for the node.
   * @param array $score_data
   *   The score data.
   * @param string $status
   *   The status.
   * @param $decoupled_uid
   * @return void
   */
  public function saveScormReport(int $nid, array $score_data, string $status, $decoupled_uid = NULL) {
   
    $account = $this->account;
    $ip = \Drupal::request()->getClientIp();    

    // If we get the uid via decoupled mode we take it and load the user
    if (isset($decoupled_uid)) {
      $user_account = $this->entityTypeManager->getStorage('user')->load($decoupled_uid);
      if ($user_account instanceof User) {
        $account = $user_account;
      }
    }
    
    // First we check for any unauthenticated user
    if ($account->id() <> 0) {
      // Define conditions
      $conditions = [
        'nid' => $nid,
        'uid' => $account->id()
      ];    

      $new_scorm_data['uid'] = $account->id();
      $new_scorm_data['nid'] = $nid;
      $new_scorm_data['score_raw'] = $score_data['raw'];
      $new_scorm_data['score_min'] = $score_data['min'];
      $new_scorm_data['score_max'] = $score_data['max'];
      $new_scorm_data['status'] = $status;

    }
    else {
      $session_uuid = $this->getSessionIDForUnauthenticatedUsers();   
      $conditions = [
        'nid' => $nid,
        'session_uuid' => $session_uuid,
        'uid' => 0,
      ];
      $new_scorm_data['session_uuid'] = $session_uuid; 
      $new_scorm_data['uid'] = 0;   
      $new_scorm_data['nid'] = $nid;
      $new_scorm_data['score_raw'] = $score_data['raw'];
      $new_scorm_data['score_min'] = $score_data['min'];
      $new_scorm_data['score_max'] = $score_data['max'];
      $new_scorm_data['status'] = $status;
    }

    // Define the storage
    $storage = $this->entityTypeManager->getStorage('scorm_report');
    $entities = $storage->loadByProperties($conditions);

    if (!empty($entities)) {
      if ($entity = reset($entities)) {
        // Make sure we save that scorm only when changed  
        if ($status === 'passed' || $status === 'completed' && $status === $entity->getStatus()) {
          $entity->setScoreRaw($score_data['raw']);
          $entity->setScoreMin($score_data['min']);
          $entity->setScoreMax($score_data['max']); 
          $entity->setNumberUpdated(1);
          $entity->save();
        }

        if ($score_data['raw'] <> $entity->getScoreRaw() ||
            $status != $entity->getStatus()
        ) {             
          $entity->setStatus($status);
          $entity->setScoreRaw($score_data['raw']);
          $entity->setScoreMin($score_data['min']);
          $entity->setScoreMax($score_data['max']); 
          
          try {
            $entity->save();
          }
          catch (\Exception $exception) {
            \Drupal::logger('scorm_field')->warning($exception->getMessage());
          }
        }
      }
    }
    else {
      // create new record
      // Add new data

      $scorm_report = $storage->create($new_scorm_data);   
      $violations = $scorm_report->validate();
      if ($violations instanceof EntityConstraintViolationList) {
        if ($violations->count() == 0) {
          $scorm_report->save();
        }
        else {
          \Drupal::logger('scorm_field')->notice('Possible Duplicate or tried to save invalid data!');
        }
      }
    }    
  }

  /**
   * Check valid score object
   *
   * @param object $scormScoreObject
   *   The scorm object
   * @return bool
   *   TRUE | FALSE
   */
  public function checkScoreObject($scormScoreObject) {

    $score = FALSE;

    if ($scormScoreObject->raw === '' &&
        $scormScoreObject->min === '' &&
        $scormScoreObject->max === '') {
      $score = FALSE;
    }
    else {
      $score = TRUE;
    }

    return $score;

  }

  /**
   * Creates and returns a unique identifier for unauthenticated users.
   *  
   * @return string $session_key
   *   Unique session key.
   */
  public function getSessionIDForUnauthenticatedUsers() {
    // Define session.
    $session = \Drupal::request()->getSession();
    // Check if we have already an unique identifier saved into session
    if (!$session->has('core.tempstore.private.owner')) {
     // This generates a unique identifier for the user
     $session->set('core.tempstore.private.owner', Crypt::randomBytesBase64());
    }
    
    $session_key = $session->get('core.tempstore.private.owner');
  
    return $session_key;
  
  }

  public function validateToken(string $token) {

    $config = \Drupal::config('scorm_field.settings');

    if ($token !== $config->get('decoupled_access_token')) {
      return FALSE;
    }

    return TRUE;

  }

  public function generateToken() {
    $bytes_value = random_bytes(8);
    $token_value = bin2hex($bytes_value);
    return $token_value;
  }

  public function saveTokenIntoDB($token) {

    $query = \Drupal::database()->insert('scorm_field_token_access');
    $query->fields([
      'access_token',
      'token_type'
    ]);
    $query->values([
      $token,
      'iframe-access-token'
    ]);  
    $query->execute();  

  }

  public function getTokenFromDB($token) {

    $query = \Drupal::database()->select('scorm_field_token_access', 's');
    $query->addField('s', 'access_token');
    $query->addField('s', 'token_type');
    $query->addField('s', 'expired');
    $query->condition('s.expired', 0);
    $query->condition('s.access_token', $token);
    $query->condition('s.token_type', 'iframe-access-token');
    $query->range(0, 1);
    $token_data = $query->execute()->fetchAssoc();
    return $token_data;

  }

  public function removeTokenFromDB($token) {
    $query = \Drupal::database()->delete('scorm_field_token_access');
    $query->condition('access_token', $token);
    $query->execute();
  }

  public function setScromPlayerSettings(NodeInterface $node, array $data) {

    $entity_type = 'node';
    $uuid = $node->uuid();
    $config_id = $entity_type . '_' . $uuid;

    // Check if we need to create new entry
    $existing_scorm_player_settings = $this->getScormPlayerSettings($node);
    if ($existing_scorm_player_settings instanceof ScormPlayerSettingsInterface) {
      foreach($data as $property => $value) {
        $existing_scorm_player_settings->{$property} = $value;        
      }
      $existing_scorm_player_settings->save();
    }
    else {
      $scorm_player_settings_storage = $this->entityTypeManager
        ->getStorage('scorm_player_settings');  
      $data['id'] = $config_id;     
      $scorm_player_settings = $scorm_player_settings_storage->create($data);
      $scorm_player_settings->save();
    }
  }

  public function getScormPlayerSettings(NodeInterface $node) {

    $entity_type = 'node';
    $uuid = $node->uuid();
    $config_id = $entity_type . '_' . $uuid;

    $scorm_player_settings = $this->entityTypeManager
      ->getStorage('scorm_player_settings')
      ->load($config_id);

    return $scorm_player_settings;

  }




}


