<?php

namespace Drupal\scorm_field;

use Drupal\Core\Database\Connection;
use Drupal\scorm_field\ScormFieldScorm;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\scorm_field\ScormPlayerSettingsInterface;
use Drupal\user\UserInterface;
use Drupal\user\Entity\User;

/**
 * Class ScormFieldScormPlayer.
 */
class ScormFieldScormPlayer {

  protected $database;

  protected $scorm_service;

  /**
   * ScormFieldScormPlayer constructor.
   */
  public function __construct(Connection $database, ScormFieldScorm $scorm_service) {
    $this->database = $database;
    $this->scorm_service = $scorm_service;
  }

  public function toRendarableArrayDecoupled($scorm, $nid, $account = NULL) {

    // Get scorm data
    $scorm_data = $this->getScormData($scorm, $nid, $account);

    return [
      '#theme' => 'scorm_field_scorm__player__decoupled',
      '#scorm_id' => $scorm_data['scorm_id'],
      '#node_id' => $scorm_data['node_id'],
      '#tree' => $scorm_data['tree'],
      '#start_sco' => $scorm_data['start_sco'],
      '#iframe_src' => $scorm_data['iframe_src'],
      '#player_settings' => $scorm_data['player_settings'],
      '#attached' => [
        'library' => ['scorm_field/scorm-field-scorm-player'],
        'drupalSettings' => [
          'scormFieldScormUIPlayer' => [
            'cmiPaths' => $scorm_data['cmi_paths'],
            'cmiData' => $scorm_data['cmi_data'],
            'scoIdentifiers' => $scorm_data['sco_identifiers'],
            'cmiSuspendItems' => $scorm_data['cmi_suspend_items'],
          ],
          'scormVersion' => $scorm_data['scorm_version'],
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];

  }

  /**
   * Get scorm data.
   *
   * @param $scorm
   *   The referenced scorm.
   * @param $nid
   *   The node id.
   * @param $account
   *   The receiving account.
   * @return 
   *   An array of scorm data.
   */
  protected function getScormData($scorm, $nid, $account = NULL): array {

    // Assume current user if no account data.
    if (!$account instanceof AccountInterface ||
        !$account instanceof UserInterface ||
        !$account instanceof User
    ) {
      $account = \Drupal::currentUser();
    }

    $player_settings = [
      'iframe_scrolling' => 'no',
      'iframe_responsive' => TRUE,
      'iframe_responsive_class' => 'responsive--iframe-default'
    ];

    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    if ($node instanceof NodeInterface) {
      $uuid = $node->uuid();
      $config_id = 'node_' . $uuid;
      $scorm_player_settings = \Drupal::entityTypeManager()->getStorage('scorm_player_settings')->load($config_id);
      if ($scorm_player_settings instanceof ScormPlayerSettingsInterface) {
        $player_settings = $scorm_player_settings;
       }
    }

    // First let's check if we can find any meta data.
    // Meta data in scorm is mandatory in version 2004,
    // so if there is none, we could assume version 1.2
    $scorm_version = '1.2';

    if (isset($scorm->metadata) && !empty($scorm->metadata)) {
      // Get SCORM API version.
      $metadata = unserialize($scorm->metadata);      
      if (strpos($metadata['schemaversion'], '1.2') !== FALSE) {
        $scorm_version = '1.2';
      }
      else {
        $scorm_version = '2004';
      }
    }

    // Get the SCO tree.
    $tree = $this->scormFieldScormPlayerScormTree($scorm);
    $flat_tree = $this->scormFieldScormPlayerFlattenTree($tree);    
    
    // Get the start SCO.
    $start_sco = $this->scormFieldScormPlayerStartSco($flat_tree);

    /* @todo Replace with custom event subscriber implementation. */
    // Get implemented CMI paths.
    $paths = scorm_field_scorm_add_cmi_paths($scorm_version);

    // Get CMI data for each SCO.
    $data = scorm_field_scorm_add_cmi_data($scorm, $flat_tree, $scorm_version, $account);

    $sco_identifiers = [];
    $scos_suspend_data = [];
    foreach ($flat_tree as $sco) {
      if ($sco->scorm_type == 'sco') {
        $sco_identifiers[$sco->identifier] = $sco->id;
        $scos_suspend_data[$sco->id] = scorm_field_default_scorm_cmi_get($account->id(), $scorm->id, 'cmi.suspend_data.' . $sco->id, '');
      }
    }
    $last_user_sco = scorm_field_default_scorm_cmi_get($account->id(), $scorm->id, 'user.sco', '');
    if ($last_user_sco != '') {
      foreach ($flat_tree as $sco) {
        if ($last_user_sco == $sco->id && !empty($sco->launch)) {
          $start_sco = $sco;
        }
      }
    }
    // Add base path for player link.
    global $base_path;
    $start_sco->base_path = $base_path;
    
    return [
      'scorm_version' => $scorm_version,
      'scorm_id' => $scorm->id,
      'cmi_paths' => $paths,
      'cmi_data' => $data,
      'sco_identifiers' => $sco_identifiers,
      'cmi_suspend_items' => $scos_suspend_data,
      'node_id' => $nid,
      'tree' => count($flat_tree) == 2 ? NULL : $tree,
      'start_sco' => $start_sco,
      'iframe_src' => $base_path . 'scorm-field-scorm/player/sco/' . $start_sco->id,
      'player_settings' => $player_settings,
    ];

  }

  /**
   * Build rendarable array for scorm package output.
   */
  public function toRendarableArray($scorm, $nid) {

    $account = \Drupal::currentUser();

    // Get scorm data
    $scorm_data = $this->getScormData($scorm, $nid, $account);   

    return [
      '#theme' => 'scorm_field_scorm__player',
      '#scorm_id' => $scorm_data['scorm_id'],
      '#node_id' => $scorm_data['node_id'],
      '#tree' => $scorm_data['tree'],
      '#start_sco' => $scorm_data['start_sco'],
      '#iframe_src' => $scorm_data['iframe_src'],
      '#player_settings' => $scorm_data['player_settings'],
      '#attached' => [
        'library' => ['scorm_field/scorm-field-scorm-player'],
        'drupalSettings' => [
          'scormFieldScormUIPlayer' => [
            'cmiPaths' => $scorm_data['cmi_paths'],
            'cmiData' => $scorm_data['cmi_data'],
            'scoIdentifiers' => $scorm_data['sco_identifiers'],
            'cmiSuspendItems' => $scorm_data['cmi_suspend_items'],
          ],
          'scormVersion' => $scorm_data['scorm_version'],
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
  
  public function getStartSCO($scorm) {
  
     // Get the SCO tree.
    $tree = $this->scormFieldScormPlayerScormTree($scorm);
    $flat_tree = $this->scormFieldScormPlayerFlattenTree($tree);

    // Get the start SCO.
    $start_sco = $this->scormFieldScormPlayerStartSco($flat_tree);
    
    return $start_sco;
  
  }

  /**
   * Traverse the SCORM package data and construct a SCO tree.
   *
   * @param object $scorm
   *   Scorm object.
   * @param int $parent_identifier
   *   Parent identifier.
   *
   * @return array
   *   SCO tree.
   */
  private function scormFieldScormPlayerScormTree($scorm, $parent_identifier = 0) {
    $conenction = $this->database;
    $tree = [];

    $result = $conenction->select('scorm_field_scorm_package_scos', 'sco')
      ->fields('sco', ['id'])
      ->condition('sco.scorm_id', $scorm->id)
      ->condition('sco.parent_identifier', $parent_identifier)
      ->execute();

    while ($sco_id = $result->fetchField()) {
      $sco = $this->scorm_service->scormLoadSco($sco_id);

      $children = $this->scormFieldScormPlayerScormTree($scorm, $sco->identifier);

      $sco->children = $children;

      $tree[] = $sco;
    }

    return $tree;
  }

  /**
   * Helper function to flatten the SCORM tree.
   *
   * @param array $tree
   *   Tree.
   *
   * @return array
   *   SCORM tree.
   */
  private function scormFieldScormPlayerFlattenTree(array $tree) {
    $items = [];

    if (!empty($tree)) {
      foreach ($tree as $sco) {
        $items[] = $sco;
        if (!empty($sco->children)) {
          $items = array_merge($items, $this->scormFieldScormPlayerFlattenTree($sco->children));
        }
      }
    }

    return $items;
  }

  /**
   * Determine the start SCO for the SCORM package.
   *
   * @todo Get last viewed SCO.
   *
   * @param array $flat_tree
   *   Flat tree.
   *
   * @return object
   *   Start SCO.
   */
  private function scormFieldScormPlayerStartSco(array $flat_tree) {
    foreach ($flat_tree as $sco) {
      if (!empty($sco->launch)) {
        return $sco;
      }
    }

    // Failsafe. Just get the first element.
    return array_shift($flat_tree);
  }

}
