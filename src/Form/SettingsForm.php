<?php

namespace Drupal\scorm_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;

/**
 * Configure scorm field settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scorm_field_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['scorm_field.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


    // Display a page description.
    $form['description'] = [
      '#markup' => '<p>' . $this->t('This page allows you to configure scorm field to operate in decoupled mode..') . '</p>',
    ];

    $form['scorm_settings_container'] = [
      '#id' => 'scorm-settings-container',
      '#type' => 'details',
      '#title' => $this->t('Scorm Settings'),
      '#description' => $this->t('You have to generate a access token to use the scorm field in a decoupled environment.'),
      '#open' => TRUE,
    ];
    
    /*
    $form['scorm_settings_container']['decoupled_mode'] = [
      '#title' => $this->t('Decoupled mode'),
      '#type' => 'checkbox',
      '#default_value' => $this->config('scorm_field.settings')->get('decoupled_mode'),
    ];
    */

    $form['scorm_settings_container']['decoupled_access_token'] = [
      '#title' => $this->t('Access token to call REST Endpoint.'),
      '#type' => 'textfield',
      '#default_value' => $this->config('scorm_field.settings')->get('decoupled_access_token') ?? '',
      '#prefix' => '<div id="token-replacement">',
      '#suffix' => '</div>',      
    ];

    $form['scorm_settings_container']['decoupled_access_token_regenerate'] = [
      '#value' => $this->t('Re-Generate token'),
      '#name' => 'regenerate_token',
      '#type' => 'button',
      '#ajax' => [
        'callback' => '::setToken',
        'wrapper' => 'token-replacement',
      ],  
      '#limit_validation_errors' => []      
    ];    

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('scorm_field.settings')
      ->set('decoupled_access_token', $form_state->getValue('decoupled_access_token'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   *
   */
  public function setToken(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $bytes_value = random_bytes(8);
    $token_value = bin2hex($bytes_value);
    // Will invoke the jQuery method .val() https://api.jquery.com/val/
    $response->addCommand(new InvokeCommand('#edit-decoupled-access-token', 'val', [$token_value]));
   
    // Return the AJAX response.
    return $response;

   }  

}
