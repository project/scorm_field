<?php

namespace Drupal\scorm_field\Plugin\rest\resource;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\scorm_field\ScormFieldCommonService;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Scorm data resource.
 *
 * @RestResource (
 *   id = "scorm_data_resource",
 *   label = @Translation("Scorm Data"),
 *   uri_paths = {
 *     "canonical" = "/api/scorm-data/{user_id}",
 *   }
 * )
 */
class ScormDataResource extends ResourceBase {

  /**
   * The key-value storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $storage;

  /**
   * ScormFieldCommonService
   */
  protected $scormFieldCommonService;  

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    KeyValueFactoryInterface $keyValueFactory,
    ScormFieldCommonService $scorm_field_common_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $keyValueFactory, $scorm_field_common_service);
    $this->storage = $keyValueFactory->get('scorm_field_scorm_report');
    $this->scormFieldCommonService = $scorm_field_common_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('keyvalue'),
      $container->get('scorm_field.common_service')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param string $user_id
   *   The UUID for the user of the record.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the record.
   */
  public function get($user_id = NULL) {
   
    $response = [];

    // Get the node ids for the material
    $nids = \Drupal::request()->query->get('nids');

    try {

      if (empty($user_id)) {
        throw new BadRequestHttpException("The user id was not given.");
      }

      if (isset($nids) && !empty($nids)) {
        $nids = json_decode($nids);
        $scorm_data = $this->scormFieldCommonService->getScormDataForGivenNids($user_id, $nids);     
        if (is_array($scorm_data)) { 
          $response['status'] = 'success';
          $response['action'] = 'scorm_data';
          $response['data'] = $scorm_data;  
          $response = new ModifiedResourceResponse($response, 201);
        }
      }
      else {
        throw new BadRequestHttpException("The nids query parameter was not given.");
      }
    }
    catch(BadRequestHttpException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 400);      

    }
    catch(EntityStorageException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 422);
          
    }
    catch(\Exception $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 422);

    }   
    
    return $response;

  }

 
}
