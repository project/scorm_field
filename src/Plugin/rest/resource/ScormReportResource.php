<?php

namespace Drupal\scorm_field\Plugin\rest\resource;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\scorm_field\ScormFieldCommonService;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\UserInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Session\AccountInterface;

/**
 * Represents Scorm Report records as resources.
 *
 * @RestResource (
 *   id = "scorm_field_scorm_report",
 *   label = @Translation("Scorm Report"),
 *   uri_paths = {
 *     "canonical" = "/api/scorm-field-scorm-report/{id}",
 *     "create" = "/api/scorm-field-scorm-report"
 *   }
 * )
 */
class ScormReportResource extends ResourceBase {

  /**
   * The key-value storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $storage;

  /**
   * ScormFieldCommonService
   */
  protected $scormFieldCommonService;  

  /**
   * Current Request
   */
  protected $request;   

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    KeyValueFactoryInterface $keyValueFactory,
    ScormFieldCommonService $scorm_field_common_service,
    Request $request
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $keyValueFactory, $scorm_field_common_service, $request);
    $this->storage = $keyValueFactory->get('scorm_field_scorm_report');
    $this->scormFieldCommonService = $scorm_field_common_service;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('keyvalue'),
      $container->get('scorm_field.common_service'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param int $id
   *   The ID of the record.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the record.
   */
  public function get($id) {

    $response = [];

    try {

      // If the current user request a report, make sure he can
      // access his own data.
      $current_user = \Drupal::currentUser();
      if ($current_user instanceof AccountInterface) {
        $user = $this->scormFieldCommonService->getUserFromAccount($current_user->id());
        if ($user instanceof UserInterface || $user instanceof User) {
          $current_user_uuid = $user->uuid();
          $reports_current_user = $this->scormFieldCommonService->getScormReportByNodeId($id, $current_user_uuid);
        }
      }

      // Check for the uid filter
      $user_uuid = $this->request->query->get('user_uuid');

      if (isset($user_uuid) && !empty($user_uuid) && is_string($user_uuid)) {
        $reports = $this->scormFieldCommonService->getScormReportByNodeId($id, $user_uuid);        
      }
      else {
        $reports = $this->scormFieldCommonService->getScormReportByNodeId($id);
      }      

      if (!empty($reports)) {
        
        if ($this->scormFieldCommonService->checkViewReportAccess($id)) {
          $response['status'] = 'success';
          $response['action'] = 'scorm_report';
          $response['object'] = $reports;  
          $response = new ModifiedResourceResponse($response, 201);
        }
        elseif ($this->scormFieldCommonService->checkViewMyReportAccess($id)) {

          if (isset($reports_current_user) && !empty($reports_current_user)) {
            $response['status'] = 'success';
            $response['action'] = 'scorm_report';
            $response['object'] = $reports_current_user;  
            $response = new ModifiedResourceResponse($response, 201);
          }
          else {
            throw new NotFoundHttpException("No data available for the user.");
          }
        }
        else {
           throw new AccessDeniedHttpException("Access Denied");
        }

      }      
      else {
        throw new NotFoundHttpException("The nid does not deliver any data.");
      }
    }
    catch(\BadRequestHttpException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 400);      

    }
    catch(\EntityStorageException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 422);
          
    }
    catch(\Exception $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 422);

    }   
    
    return $response;

  }

 
}
