<?php

namespace Drupal\scorm_field\Plugin\rest\resource;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\scorm_field\ScormFieldCommonService;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Scorm decoupled access token resource.
 *
 * @RestResource (
 *   id = "scorm_decoupled_access_token_resource",
 *   label = @Translation("Scorm Decoupled Access Token"),
 *   uri_paths = {
 *     "canonical" = "/api/scorm-field-scorm-get-access-token/{token}/{node_id}/{user_id}",
 *   }
 * )
 */
class ScormDecoupledAccessTokenResource extends ResourceBase {

  /**
   * The key-value storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $storage;

  /**
   * ScormFieldCommonService
   */
  protected $scormFieldCommonService;  

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    KeyValueFactoryInterface $keyValueFactory,
    ScormFieldCommonService $scorm_field_common_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $keyValueFactory, $scorm_field_common_service);
    $this->storage = $keyValueFactory->get('scorm_field_scorm_report');
    $this->scormFieldCommonService = $scorm_field_common_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('keyvalue'),
      $container->get('scorm_field.common_service')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @param int $id
   *   The ID of the record.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the record.
   */
  public function get($token, $node_id, $user_id = NULL) {
   
    $response = [];

    // We assume 0 for user id per default
    $current_user = 0;

    try {

      if (isset($token) && empty($token)) {
        throw new BadRequestHttpException("The client has not defined the access token.");
      }

      if (isset($node_id) && empty($node_id)) {
        throw new BadRequestHttpException("The client has not defined the node id.");
      }

      if (!empty($user_id)) {
        $user = \Drupal::service('entity.repository')->loadEntityByUuid('user', $user_id);
        if ($user instanceof UserInterface) {
          $current_user = $user->id();
        }      
      }

      $token_valid = $this->scormFieldCommonService->validateToken($token, $node_id);

      if ($token_valid) { 

        // When token valid we want to generate an access token and save it into the DB
        $generated_token = $this->scormFieldCommonService->generateToken();
        // We want to save this token now
        try {
          $this->scormFieldCommonService->saveTokenIntoDB($generated_token);
          $options = [
            'absolute' => TRUE,
            'query' => ['uid' => $current_user]
          ];
          $decoupled_url = Url::fromRoute('scorm_field.decoupled', ['node' => $node_id, 'token' => $generated_token], $options);
          $response['status'] = 'success';
          $response['action'] = 'token generated';
          $response['generated_token'] = $generated_token;  
          $response['iframe_source'] = $decoupled_url->toString();  
          $response = new ModifiedResourceResponse($response, 201);
        }
        catch (\Exception $e) {
          $this->logger->warning($e->getMessage());
          $error['error'] = $e->getMessage();
          $response = new ModifiedResourceResponse($error, 422); 
        } 
      }      
      else {
        throw new BadRequestHttpException("The client is missing the access token or the token is not valid.");
      }
    }
    catch(BadRequestHttpException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 400);      

    }
    catch(EntityStorageException $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 422);
          
    }
    catch(\Exception $e) {

      $this->logger->warning($e->getMessage());
      $error['error'] = $e->getMessage();
      $response = new ModifiedResourceResponse($error, 422);

    }   
    
    return $response;

  }

 
}
