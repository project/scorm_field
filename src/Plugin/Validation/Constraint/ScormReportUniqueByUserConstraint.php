<?php

declare(strict_types=1);

namespace Drupal\scorm_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a ScormReportUniqueByUser constraint.
 *
 * @Constraint(
 *   id = "ScormFieldScormReportUniqueByUser",
 *   label = @Translation("ScormReportUniqueByUser", context = "Validation"),
 * )
 *
 * @see https://www.drupal.org/node/2015723.
 */
final class ScormReportUniqueByUserConstraint extends Constraint {

  public string $notUnique = '@todo Specify error message here.';

}
