<?php

declare(strict_types=1);

namespace Drupal\scorm_field\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\scorm_field\ScormReportInterface;

/**
 * Validates the ScormReportUniqueByUser constraint.
 */
final class ScormReportUniqueByUserConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Constructs the object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint): void {

    // Next check if the value is unique.
    if (!$this->isUnique($entity)) {
      $this->context->addViolation($constraint->notUnique);
    }
    
  }

  /**
   * Is unique?
   *
   * @param string $value
   */
  private function isUnique($entity) {

    $unique = TRUE;

    $storage = $this->entityTypeManager->getStorage('scorm_report');
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('nid', $entity->getNodeId());

    if ($entity->getOwnerId() == 0) {    
      $query->condition('session_uuid', $entity->getSessionUuid());
    }
    else {
      $query->condition('uid', $entity->getOwnerId());
    }

    $ids = $query->execute();    

    if (!empty($ids) && count($ids) > 0) {
      $unique = FALSE;
    }
  
    return $unique;     
    
  }
}
