<?php

namespace Drupal\scorm_field\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ScormReport Entity.
 */
class ScormReportUniqueConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * Creates a new UniqueOriginalIdConstraintValidator instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  } 

  /**
   * Checks if user_session_id and nid is unique.
   *
   * @param mixed $field
   *   The field/value that should be validated.
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   The constraint for the validation.
   */
  public function validate(mixed $entity, Constraint $constraint) {

    if ($entity) {
      $uid = $entity->getOwnerId();
      $session_uuid = $entity->session_uuid;
      $nid = $entity->getNodeId();

      $storage = $this->entityTypeManager->getStorage('scorm_report');
      $query = $storage->getQuery()
        ->accessCheck(FALSE)
        ->condition('nid', $nid);      

      if ($uid == 0) {             
        $query->condition('session_uuid', $session_uuid);
      }
      else {
        $query->condition('uid', $uid);
      }

      if (!$entity->isNew()) {
        $query->condition('id', $entity->id(), '!='); 
      }      

      $ids = $query->execute();

      if (!empty($ids)) {

        if (!$entity->isNew()) {
          $duplicate_records = count($ids) === 1;
        }
        else {
          $duplicate_records = count($ids) > 0;
        } 
  
        if (isset($ids) && !empty($ids) && $duplicate_records) {
          $this->context->buildViolation($constraint->errorMessage,['%value' => 'Combination of uid, session_uuid and nid not unique.'])
              ->addViolation();
        }
      }
    }
  }
}

