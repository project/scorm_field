<?php

namespace Drupal\scorm_field\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the record for anon user is unique.
 *
 * @Constraint(
 *   id = "ScormReportUnique",
 *   label = @Translation("Unique Session uuid for report", context = "Validation")
 * )
 */
class ScormReportUniqueConstraint extends Constraint {

  // The message that will be shown if the value is not unique.
  public $errorMessage = '%value is not unique';

}

