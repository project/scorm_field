<?php

namespace Drupal\scorm_field\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\scorm_field\ScormPlayerSettingsInterface;

/**
 * Defines the scorm player settings entity type.
 *
 * @ConfigEntityType(
 *   id = "scorm_player_settings",
 *   label = @Translation("Scorm player settings"),
 *   label_collection = @Translation("Scorm player settings"),
 *   label_singular = @Translation("Scorm player settings"),
 *   label_plural = @Translation("Scorm player settings"),
 *   label_count = @PluralTranslation(
 *     singular = "@count scorm player setting",
 *     plural = "@count scorm player settings",
 *   ),
 *   handlers = {},
 *   config_prefix = "scorm_player_settings",
 *   links = {},
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "iframe_scrolling",
 *     "iframe_responsive",
 *     "iframe_responsive_class",
 *     "iframe_height",
 *     "iframe_width",
 *     "uuid"
 *   }
 * )
 */
class ScormPlayerSettings extends ConfigEntityBase implements ScormPlayerSettingsInterface {

  /**
   * {@inheritdoc}
   */
  public function getIframeScrolling() {
    return $this->iframe_scrolling;
  }

  /**
   * {@inheritdoc}
   */
  public function setIframeScrolling(string $iframe_scrolling) {
    $this->iframe_scrolling = $iframe_scrolling;
    return $this;
  }    

  /**
   * {@inheritdoc}
   */
  public function getIframeResponsive() {
    return $this->iframe_responsive;
  }

  /**
   * {@inheritdoc}
   */
  public function getIframeResponsiveClass() {
    return $this->iframe_responsive_class;
  }

  /**
   * {@inheritdoc}
   */
  public function setIframeResponsive(bool $iframe_responsive) {
    $this->iframe_responsive = $iframe_responsive;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setIframeResponsiveClass(string $iframe_responsive_class) {
    $this->iframe_responsive_class = $iframe_responsive_class;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getIframeHeight() {
    return $this->iframe_height;
  }

  /**
   * {@inheritdoc}
   */
  public function setIframeHeight(string $iframe_height) {
    $this->iframe_height = $iframe_height;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getIframeWidth() {
    return $this->iframe_width;
  }

  /**
   * {@inheritdoc}
   */
  public function setIframeWidth(string $iframe_width) {
    $this->iframe_width = $iframe_width;
    return $this;
  }  

  /**
   * The member sync ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The iframe responsive.
   *
   * @var bool
   */
  public $iframe_responsive;

  /**
   * The iframe responsive class.
   *
   * @var string
   */
  public $iframe_responsive_class;  

  /**
   * The iframe scrolling.
   *
   * @var string
   */
  public $iframe_scrolling;  

  /**
   * The iframe height.
   *
   * @var string
   */
  public $iframe_height;  
  
  /**
   * The iframe width.
   *
   * @var string
   */
  public $iframe_width;     

}
