<?php

namespace Drupal\scorm_field\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Render\RenderContext;
use Drupal\scorm_field\ScormFieldScorm;
use Drupal\scorm_field\ScormFieldScormPlayer;

/**
 * Returns responses for Scorm field routes.
 */
class ScormFieldDecoupledController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The scorm service.
   *
   * @var \Drupal\scorm_field\ScormFieldScorm
   */
  protected $scorm;

  /**
   * The scorm player service.
   *
   * @var \Drupal\scorm_field\ScormFieldScormPlayer
   */
  protected $scormPlayer;  
  
  

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user. 
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager, 
    AccountInterface $account,
    ScormFieldScorm $scorm,
    ScormFieldScormPlayer $scorm_player    
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
    $this->scorm = $scorm;
    $this->scormPlayer = $scorm_player;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('scorm_field.scorm'),
      $container->get('scorm_field.scorm_player')
    );
  }

  /**
   * Builds the response.
   */
  public function buildScormPlayer($node, $token) {

    $build['scorm_player']['#markup'] = $this->t('No scorm material.');    
   
    $account = NULL;

    // Get Account ID from queryString uid.
    if ($uid = \Drupal::request()->query->get('uid')) {
      $account = $this->entityTypeManager->getStorage('user')->load($uid);
    }

    if ($node instanceof NodeInterface) {
      $field_definitions = $node->getFieldDefinitions();
      foreach ($field_definitions as $machine_name => $field) {
        $className = $field->getClass();
        if ($className === '\Drupal\scorm_field\Plugin\Field\FieldType\ScormFieldScormPackageItemList') {
          $existingData = $node->$machine_name->referencedEntities();
          if(!empty($existingData)) {
            $scorm_file = $node->$machine_name->entity;
            $scorm = $this->scorm->scormLoadByFileEntity($scorm_file);
            $scorm_player = $this->scormPlayer->toRendarableArrayDecoupled($scorm, $node->id(), $account);
            $build['scorm_player'] = $scorm_player;
          }
        }        
      }
    }
   
    return $build;
  
  }

  /**
   * Returns a page title.
   */
  public function getTitle($node) {
    if ($node instanceof NodeInterface) {
      return  $node->getTitle();
    }
  }  


  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, $node, $token) {

    $common_service = \Drupal::service('scorm_field.common_service');

    // Check if the token exists in DB
    $token_data = $common_service->getTokenFromDB($token);

    if ($token_data) {
      // Delete token now
      $common_service->removeTokenFromDB($token);
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();

    
  }

}
