<?php

namespace Drupal\scorm_field;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a scorm settings entity type.
 */
interface ScormPlayerSettingsInterface extends ConfigEntityInterface {

  /**
   * getIframeResponsive
   * @return bool $iframe_responsive
   */
  public function getIframeResponsive();

  /**
   * getIframeResponsiveClass
   * @return bool $iframe_responsive_class
   */
  public function getIframeResponsiveClass();  

  /**
   * setIframeResponsive
   * 
   * @param bool $iframe_responsive
   *   Iframe resonsive yes or no.
   * @return bool $iframe_responsive
   *   TRUE | FALSE
   */
  public function setIframeResponsive(bool $iframe_responsive);

  /**
   * setIframeResponsiveClass
   * 
   * @param string $iframe_responsive_class
   *   Iframe resonsive class name
   * @return string $iframe_responsive_class
   *   The selected iframe responsive class.
   */
  public function setIframeResponsiveClass(string $iframe_responsive_class);  

  /**
   * getIframeScrolling
   * 
   * @return string $iframe_scrolling
   *   The supported scrolling method for the iframe.
   *   auto, no or yes.
   */
  public function getIframeScrolling();

  /**
   * setIframeScrolling
   * 
   * @param string $iframe_scrolling
   *   The supported iframe scrolling method.
   * @return string $iframe_scrolling
   *   The supported iframe scrolling method.
   */
  public function setIframeScrolling(string $iframe_scrolling);  

  /**
   * getIframeWidth
   * 
   * @return string $iframe_width
   *   The width of the iframe
   */
  public function getIframeWidth();

  /**
   * setIframeWidth
   * 
   * @param string $iframe_width
   *   The width of the iframe.
   * @return string $iframe_width
   *   The width of the iframe.
   */
  public function setIframeWidth(string $iframe_width);   

  /**
   * getIframeHeight
   * 
   * @return string $iframe_height
   *   The height of the iframe
   */
  public function getIframeHeight();

  /**
   * setIframeHeight
   * 
   * @param string $iframe_height
   *   The height of the iframe.
   * @return string $iframe_height
   *   The height of the iframe.
   */
  public function setIframeHeight(string $iframe_height);   

}