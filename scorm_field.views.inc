<?php

/**
 * Implements hook_views_data().
 */
function scorm_field_views_data() {
  $data = [];
  $data['scorm_field_scorm_cmi_data'] = [];

  // Describe a single database table named news_subs.
  $data['scorm_field_scorm_cmi_data']['table'] = [
    // Human readable name of this table used in the Views UI to prefix fields,
    // filters, etc. Example: "News subscriptions: Email". This string should
    // be translatable.
    'group' => t('Scorm field cmi data'),
    
    // Name of the module that provides the table schema.
    'provider' => 'scorm_field',
    
    // A table can be a "base" table, meaning that in Views you can use it as
    // base for a View. Non-base tables can be associated to a base table via
    // a relationship. The primary table for your custom data should be a base
    // table. Add the "base" key with the following properties:
    
    'base' =>  [
      // Identifier (primary) field in this table for Views.
      'field' => 'nid',
      // Label in the UI.
      'title' => t('Scorm field cmi data'),
      // Longer description in the UI. Required.
      'help' => t('Scorm field cmi data table.'),
    ]
    
  ];

  // Other top level elements of the news_subs array define the individual
  // columns of the table that you want to make available to Views. The key is
  // the name (and must be unique) used by Views. It's usually the same as the
  // name of the database column it describes. But doesn't have to be. It's
  // possible to created computed fields that are not a one-to-one relationship
  // to a column in the database. For example, a field that contains a link to
  // edit a record from the table.
  //
  // The 'title' and 'help' elements are required.
  //
  // Each field definition needs to describe the views plugins (frequently
  // called "handlers") that are responsible for handling the fields data in
  // different scenarios including: field, filter, sort, argument, area, and
  // relationship. All of which are optional.
  $data['scorm_field_scorm_cmi_data']['uid'] = [
    // Human readable name of the field that will be displayed in the UI.
    'title' => t('User ID'),
    // Required help text that describes the content of the field.
    'help' => t('The User id.'),
    // Optional handler to use when displaying a field. This maps to what a user
    // will see in the "Fields" section of the Views configuration UI. Specify
    // this if you want a user to be able to display the content of this field.
    'field' => [
      // ID of field handler plugin to use. More information about this below.
      'id' => 'standard',
    ],
    // Optional handler to use when sorting field data. This maps to what a user
    // will see in the "Sort criteria" section of the Views configuration UI.
    // Specify this if you want a user to be able to sort a Views results based
    // on the content of this field.
    'sort' => [
      'id' => 'standard',
    ],
    // Optional handler to use when filtering results based on a field. This
    // maps to what a user will see in the "Filter criteria" section of the
    // Views configuration UI. Specify this if you want a user to be able to
    // filter a Views results based on the content of this field.
    'filter' => [
      'id' => 'string',
    ],
    // Optional handler to use when making this field available as an argument.
    // This maps to what a user will see in the "Contextual filters" section of
    // the Views configuration UI. Specify this if you want a user to be able to
    // use this field in contextual filters.
    'argument' => [
      'id' => 'string',
    ],
  ];

  // More examples of field descriptions.
  $data['scorm_field_scorm_cmi_data']['scorm_id'] = [
    'title' => t('Scorm ID'),
    'help' => t('The scorm material id.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $data['scorm_field_scorm_cmi_data']['cmi_key'] = [
    'title' => t('CMI Key'),
    'help' => t('The CMI Key of the scorm.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $data['scorm_field_scorm_cmi_data']['value'] = [
    'title' => t('Value'),
    'help' => t('CMI Key value.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'standard',
    ],
  ];

  $data['scorm_field_scorm_cmi_data']['user_session_id'] = [
    'title' => t('User session ID'),
    'help' => t('User session id of the scorm.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'standard',
    ],
  ];  

  $data['scorm_field_scorm_cmi_data']['nid'] = [
    'title' => t('Node ID'),
    'help' => t('Node id the scorm is attached to.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'standard',
    ],
  ];

  return $data;

}

